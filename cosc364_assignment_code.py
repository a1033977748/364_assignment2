def main():
    demand = ''
    s_t_capacity = ''
    t_d_capacity = ''
    split_paths = ''
    equal_demand_value = ''
    load_balance = ''
    bounds = ''
    binarys = ''
    demand_result_list = []
    s_t_capacity_result_list = []
    t_d_capacity_result_list = []
    split_paths_result_list = []
    equal_demand_value_list = []
    load_balance_list = []
    bounds_list = []
    binarys_list = []
    # Input variables for X, Y, Z
    try:
        x = int(input("Please input the number of source node:"))
    except:
        print("X Input error!")
        return
    try:
        y = int(input("Please input the number of transit node:"))
    except:
        print("Y Input error!")
        return
    try:
        z = int(input("Please input the number of destination node:"))
    except:
        print("Z Input error!")
        return
    # Write all constrains (demand, capacity...)
    # demand
    if x != None and y != None and z != None:
        for i in range(x):
            for j in range(z):
                for k in range(y):
                    demand += "x" + str(i+1)+ str(k+1)+ str(j+1) + " + "
                demand_result = demand[0:-2] + "= " + str(i+1 + j+1)
                demand_result_list.append(demand_result)
                demand_result = ''
                demand = ''
        print(demand_result_list)
        # source to transit capacity
        for i in range(x): 
            for k in range(y):
                for j in range(z):
                    s_t_capacity += 'x' + str(i+1)+ str(k+1)+ str(j+1) + " + "
                s_t_capacity_result = s_t_capacity[0:-2] + "- " + 'c' + str(i+1) + str(k+1) + ' <= 0'
                s_t_capacity_result_list.append(s_t_capacity_result)
                s_t_capacity_result = ''
                s_t_capacity = ''
        print(s_t_capacity_result_list)
        # transit to destiantion capacity
        for k in range(y):
            for j in range(z):
                for i in range(x): 
                    t_d_capacity += 'x' + str(i+1)+ str(k+1)+ str(j+1) + " + "
                t_d_capacity_result = t_d_capacity[0:-2] + "- " + 'd' + str(k+1) + str(j+1) + ' <= 0'
                t_d_capacity_result_list.append(t_d_capacity_result)
                t_d_capacity_result = ''
                t_d_capacity = ''
        print(t_d_capacity_result_list)
        #each demand volume shall be split over exactly two different paths
        for i in range(x):
            for j in range(z):
                for k in range(y):
                    split_paths += 'u' + str(i+1)+ str(k+1)+ str(j+1) + " + "
                split_paths_result = split_paths[0:-2] + '= 2'
                split_paths_result_list.append(split_paths_result)
                split_paths_result = ''
                split_paths = ''
        print(split_paths_result_list)
        # such that each path gets an equal share of the demand value
        for i in range(x):
            for j in range(z):
                for k in range(y):
                    # 2 x_ikj  - h_ij u_ikj = 0
                    equal_demand_value = '2' + ' ' + 'x' + str(i+1)+ str(k+1)+ str(j+1) + " - " +  str(i+1 + j+1) + ' ' + 'u' + str(i+1)+ str(k+1)+ str(j+1) + ' = 0'
                    equal_demand_value_list.append(equal_demand_value)
                    equal_demand_value = ''
        print(equal_demand_value_list)
        # load balance
        for k in range(y):
            for i in range(x):
                for j in range(z):
                    # x_111 + x_211 + x_212 <= r
                    load_balance += 'x' + str(i+1)+ str(k+1)+ str(j+1) + ' + '

            load_balance_result = load_balance[0:-2] + '- ' + 'r' + ' <= 0'
            load_balance_list.append(load_balance_result)
            load_balance = ''
            load_balance_result = ''
        print(load_balance_list)
        # bounds
        for i in range(x):
            for j in range(z):
                for k in range(y):
                    bounds = '0 <= ' + 'x' + str(i+1)+ str(k+1)+ str(j+1) 
                    bounds_list.append(bounds)
                    bounds = ''
        bounds_list.append('0 <= r')
        print(bounds_list)
        # binary
        for i in range(x):
            for j in range(z):
                for k in range(y):
                    binarys = 'u' + str(i+1)+ str(k+1)+ str(j+1) 
                    binarys_list.append(binarys)
                    binarys = ''
        print(binarys_list)

    # Creat LP file
    file_name = "OutPut.lp"
    open_file = open(file_name, 'w')
    open_file.write("Minimize\n r\nSubject to\n")

    #write demand constrain
    open_file.write(' demandConstrain: \n')
    for demand in demand_result_list:
        open_file.write(' ')
        open_file.write(demand)
        open_file.write('\n')

    #write s_t_capacity_result_list
    open_file.write(' s_t_capacity: \n')
    for s_t_capacity in s_t_capacity_result_list:
        open_file.write(' ')
        open_file.write(s_t_capacity)
        open_file.write('\n')
    
    #write t_d_capacity_result_list
    open_file.write(' t_d_capacity: \n')
    for t_d_capacity in t_d_capacity_result_list:
        open_file.write(' ')
        open_file.write(t_d_capacity)
        open_file.write('\n')

    #write each demand volume shall be split over exactly two different paths
    open_file.write(' split path: \n')
    for split_paths in split_paths_result_list:
        open_file.write(' ')
        open_file.write(split_paths)
        open_file.write('\n')

    #write such that each path gets an equal share of the demand value
    open_file.write(' equal demand: \n')
    for equal_demand in equal_demand_value_list:
        open_file.write(' ')
        open_file.write(equal_demand)
        open_file.write('\n') 

    #write load balance
    open_file.write(' load balance: \n')
    for load_balance in load_balance_list:
        open_file.write(' ')
        open_file.write(load_balance)
        open_file.write('\n') 
    
    #write bounds
    open_file.write('Bounds\n')
    for bounds in bounds_list:
        open_file.write(' ')
        open_file.write(bounds)
        open_file.write('\n') 
    
    #write binarys
    open_file.write('Binary\n')
    for binarys in binarys_list:
        open_file.write(' ')
        open_file.write(binarys)
        open_file.write('\n') 

    open_file.write('End')

    open_file.close()

if __name__ == '__main__':
    main()